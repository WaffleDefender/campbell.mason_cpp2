#include "Turret.h"

void Turret::Draw(Core::Graphics & graphics)
{
	graphics.DrawLine(m_turret[0].x, m_turret[0].y, m_turret[1].x, m_turret[1].y);
}

void Turret::Update(Vec2 position, Vec2 velocity)
{
	m_turret[1] = position + velocity;
	m_turret[0] = position;
	if (m_keyboard.MouseRightPressed()) {
		m_turret[1] = ((Vec2{ (float)Core::Input::GetMouseX(), (float)Core::Input::GetMouseY() }) - position).Normalize() * 10.0f + position;
	}
}

void Turret::Initialize(Vec2 position)
{
	m_turret[0] = position;
	m_turret[1] = position - Vec2{ 0, 10 };
}
