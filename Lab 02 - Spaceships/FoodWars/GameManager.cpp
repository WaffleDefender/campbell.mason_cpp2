#include "GameManager.h"
#include "Core.h"

GameManager* GameManager::m_manager;

GameManager::GameManager(int width, int height)
	:
	m_screenWidth(width),
	m_screenHeight(height),
	m_spaceship(450, 300)
{
	Initialize();
}

GameManager::~GameManager()
{
	Shutdown();
}

bool GameManager::RunGame()
{
	Core::GameLoop();
	return true;
}

bool GameManager::Update(float dt)
{
	Keyboard keyboard;
	m_fps.CalcFPS(dt);
	m_spaceship.Update(dt);
	m_enemyship.Update(dt);
	if (keyboard.KeyEscapePressed()) return true;
	return false;
}

void GameManager::Draw(Core::Graphics & graphics)
{
	m_fps.DisplayFPS(graphics);
	m_spaceship.Draw(graphics, m_spaceship);
	m_enemyship.Draw(graphics, m_enemyship);
}

bool GameManager::Initialize()
{
	m_manager = this;
	m_isInitialized = true;
	Core::Init("Food Wars", m_screenWidth, m_screenHeight, 2500);
	Core::RegisterUpdateFn(UpdateCallBack);
	Core::RegisterDrawFn(DrawCallBack);
	return true;
}

bool GameManager::Shutdown()
{
	Core::Shutdown();
	return true;
}
