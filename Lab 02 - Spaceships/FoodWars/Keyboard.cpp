#include "Keyboard.h"
#include "Core.h"

Vec2 Keyboard::VelocityUp(float dt)
{
	return Vec2{ 0, -1} * (dt * m_speed);
}

Vec2 Keyboard::VelocityDown(float dt)
{
	return Vec2{ 0, 1 } *(dt * m_speed);
}

Vec2 Keyboard::VelocityRight(float dt)
{
	return Vec2{ 1, 0 } *(dt * m_speed);
}

Vec2 Keyboard::VelocityLeft(float dt)
{
	return Vec2{ -1, 0 } *(dt * m_speed);
}

bool Keyboard::KeyUpPressed()
{
	return (Core::Input::IsPressed(Core::Input::KEY_UP)) ? true : false;
}

bool Keyboard::KeyDownPressed()
{
	return (Core::Input::IsPressed(Core::Input::KEY_DOWN)) ? true : false;
}

bool Keyboard::KeyLeftPressed()
{
	return (Core::Input::IsPressed(Core::Input::KEY_LEFT)) ? true : false;
}

bool Keyboard::KeyRightPressed()
{
	return (Core::Input::IsPressed(Core::Input::KEY_RIGHT)) ? true : false;
}

bool Keyboard::KeyNumPadOnePressed()
{
	return (Core::Input::IsPressed(VK_NUMPAD1)) ? true : false;
}

bool Keyboard::KeyNumPadTwoPressed()
{
	return (Core::Input::IsPressed(VK_NUMPAD2)) ? true : false;
}

bool Keyboard::KeyNumPadFourPressed()
{
	return (Core::Input::IsPressed(VK_NUMPAD4)) ? true : false;
}

bool Keyboard::KeyNumPadFivePressed()
{
	return (Core::Input::IsPressed(VK_NUMPAD5)) ? true : false;
}

bool Keyboard::KeyEscapePressed()
{
	return (Core::Input::IsPressed(Core::Input::KEY_ESCAPE)) ? true : false;
}

bool Keyboard::MouseRightPressed()
{
	return (Core::Input::IsPressed(Core::Input::BUTTON_RIGHT)) ? true: false;
}
