#pragma once
#include "Vec2.h"
class Keyboard
{
public:
	Vec2 VelocityUp(float dt);
	Vec2 VelocityDown(float dt);
	Vec2 VelocityRight(float dt);
	Vec2 VelocityLeft(float dt);

	bool KeyUpPressed();
	bool KeyDownPressed();
	bool KeyLeftPressed();
	bool KeyRightPressed();
	bool KeyNumPadOnePressed();
	bool KeyNumPadTwoPressed();
	bool KeyNumPadFourPressed();
	bool KeyNumPadFivePressed();
	bool KeyEscapePressed();
	bool MouseRightPressed();

private:
	float m_speed = 400.0f;
};

