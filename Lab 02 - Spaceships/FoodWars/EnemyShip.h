#pragma once
#include "Core.h"
#include "Vec2.h"
#include "Keyboard.h"
class EnemyShip
{
public:
	EnemyShip();
	bool Update(float dt);

	float GetPercentage();

	void Draw(Core::Graphics& graphics, EnemyShip& ship);
	void DrawPath(Core::Graphics& graphics, EnemyShip& ship); 
	void DrawLine(Core::Graphics & graphics, const Vec2 & p1, const Vec2 & p2);
	void InitializePoints();
	void InitializePath();
	void CheckPath();

private:
	Keyboard m_keyboard;

	Vec2 m_enemyship[9];
	Vec2 m_enemyposition;
	Vec2 m_enemypath[5];

	bool m_side1And2 = true;
	bool m_side3And4 = true;
	bool m_showpath = true;

	float m_timer = 0;
	float m_percent = 0;
	float m_enemyspeed = 50;
};

