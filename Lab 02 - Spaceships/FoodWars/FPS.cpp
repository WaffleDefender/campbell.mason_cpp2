#include "FPS.h"
#include "GameManager.h"


void FPS::CalcFPS(float dt)
{
	static float timeSpan = 0.0f;
	static int numFrames = 0;
	const int fraquency = 1;
	const float freqSpan = 1.0f / fraquency;

	++numFrames;
	timeSpan += dt;
	if (timeSpan >= freqSpan)
	{
		m_fps = numFrames*fraquency;
		numFrames = 0;
		timeSpan -= freqSpan;
	}
}

void FPS::DisplayFPS(Core::Graphics & graphics)
{
	const int bufSize = 20;
	char timebuf[bufSize];
	sprintf_s(timebuf, bufSize, "FPS: %d", m_fps);
	graphics.SetColor(RGB(0, 255, 255));
	graphics.DrawString(GameManager::GetScreenWidth() - 100, 0, timebuf);
}
