#pragma once
#include "Core.h"
#include "Vec2.h"
class Wall
{
public:	
	Vec2 Update(Vec2 velocity, Vec2 shipposition);
	Vec2 CheckWall(Vec2 &shipposition, Vec2 &velocity, Vec2 &wallpoint, Vec2 &wallpoint2);
	
	void Draw(Core::Graphics& graphics, Wall& wall);
	void Initialize(Vec2 position);

private:
	Vec2 m_wallshape[5];
	Vec2 m_wallposition;
};

