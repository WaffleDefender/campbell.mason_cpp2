#pragma once

#include "Core.h"
#include "Vec2.h"
#include "Keyboard.h"
#include "Turret.h"
#include "EnemyShip.h"
#include "Wall.h"
class Spaceship
{
public:
	Spaceship(float x, float y);
	bool Update(float dt);
	void Draw(Core::Graphics& graphics, Spaceship& ship);
	void DrawLine(Core::Graphics & graphics, const Vec2 & p1, const Vec2 & p2);
	void InitializePoints();

private:
	void CheckShipAction();
	void WrapShip();
	void BounceShip();

private:
	Vec2 m_shipShape[11];
	Vec2 m_position;
	Vec2 m_velocity;

	Keyboard m_keyboard;
	Wall m_wall;
	Turret m_turret;

	bool m_bounce = false;
	bool m_wrap = true;

};

