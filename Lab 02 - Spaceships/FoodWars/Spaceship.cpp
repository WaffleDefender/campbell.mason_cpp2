#include "Spaceship.h"
#include "GameManager.h"
#include <sstream>

Spaceship::Spaceship(float x, float y)
{
	m_position = { x, y };
	InitializePoints();
	m_turret.Initialize(m_position);
	m_wall.Initialize({ 800 / 2, 800 / 2 });
	printf("\n(1), 2 : "); printf((m_wrap) ? "Wrap" : "Bounce");
}

bool Spaceship::Update(float dt)	
{
	m_velocity = { 0, 0 };
	if (m_keyboard.KeyUpPressed()) {
		m_velocity = m_keyboard.VelocityUp(dt);
		m_position = m_position + m_velocity;
	}
	if (m_keyboard.KeyDownPressed()) {
		m_velocity = m_keyboard.VelocityDown(dt);
		m_position = m_position + m_velocity;
	}
	if (m_keyboard.KeyRightPressed()) {
		m_velocity = m_keyboard.VelocityRight(dt);
		m_position = m_position + m_velocity;
	}
	if (m_keyboard.KeyLeftPressed()) {
		m_velocity = m_keyboard.VelocityLeft(dt);
		m_position = m_position + m_velocity;
	}
	CheckShipAction();
	m_turret.Update(m_position, m_velocity);
	m_position = m_wall.Update(m_velocity, m_position);
	return false;
}

void Spaceship::Draw(Core::Graphics & graphics, Spaceship & ship)
{
	graphics.SetColor(RGB(0, 255, 255));
	for (int i = 0; i < 10; ++i)
	{
		Vec2 point1 = ship.m_shipShape[i] + ship.m_position;
		Vec2 point2 = ship.m_shipShape[i + 1] + ship.m_position;
		ship.DrawLine(graphics, point1, point2);
	}
	m_turret.Draw(graphics);
	m_wall.Draw(graphics, m_wall);
}

void Spaceship::DrawLine(Core::Graphics & graphics, const Vec2 & p1, const Vec2 & p2)
{
	graphics.DrawLine(p1.x, p1.y, p2.x, p2.y);
}

void Spaceship::InitializePoints()
{
	m_shipShape[0] = Vec2{ -10,0 };
	m_shipShape[1] = Vec2{ 0,-15 };
	m_shipShape[2] = Vec2{ 0,-15 };
	m_shipShape[3] = Vec2{ 10,0 };
	m_shipShape[4] = Vec2{ 10,0 };
	m_shipShape[5] = Vec2{ 0,15 };
	m_shipShape[6] = Vec2{ 0,15 };
	m_shipShape[7] = Vec2{ -10,0 };
	m_shipShape[8] = Vec2{ -15,15 };
	m_shipShape[9] = Vec2{ 15, 15 };
	m_shipShape[10] = m_shipShape[4];
}

void Spaceship::CheckShipAction()
{
	if (m_keyboard.KeyNumPadOnePressed() && m_bounce && !m_wrap) {
		m_bounce = false;
		m_wrap = true;
		printf("\n(1), 2 : "); printf((m_wrap) ? "Wrap" : "Bounce");
	}
	else if (m_keyboard.KeyNumPadTwoPressed() && !m_bounce && m_wrap) {
		m_bounce = true;
		m_wrap = false;
		printf("\n1, (2) : ");printf((m_bounce) ? "Bounce" : "Wrap");
	}
	if (m_wrap) {
		WrapShip();
	}
	else if(m_bounce){
		BounceShip();
	}
}

void Spaceship::WrapShip()
{
	if (m_position.x >= GameManager::GetScreenWidth()) {
		m_position = Vec2{ 0, m_position.y };
	}
	else if (m_position.x <= 0) {
		m_position = Vec2{ (float)GameManager::GetScreenWidth(), m_position.y };
	}
	else if (m_position.y >= GameManager::GetScreenHeight()) {
		m_position = Vec2{ m_position.x, 0 };
	}
	else if (m_position.y <= 0) {
		m_position = Vec2{ m_position.x, (float)GameManager::GetScreenHeight() };
	}
}

void Spaceship::BounceShip()
{
	if (m_position.x >= GameManager::GetScreenWidth()) {
		m_position = Vec2{ (float) GameManager::GetScreenWidth(), m_position.y };
	}
	if (m_position.x <= 0) {
		m_position = Vec2{ 0, m_position.y };
	}
	if (m_position.y >= GameManager::GetScreenHeight()) {
		m_position = Vec2{ m_position.x, (float)GameManager::GetScreenHeight() };
	}
	if (m_position.y <= 0) {
		m_position = Vec2{ m_position.x, 0 };
	}
}
