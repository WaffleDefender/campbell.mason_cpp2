#pragma once
#include "Core.h"
#include "Vec2.h"
#include "Keyboard.h"
class Turret
{
public:
	void Draw(Core::Graphics & graphics);
	void Update(Vec2 position, Vec2 velocity);
	void Initialize(Vec2 position);

private:
	Vec2 m_turret[2];
	Keyboard m_keyboard;
};

