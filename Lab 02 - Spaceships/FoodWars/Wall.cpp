#include "Wall.h"
#include "GameManager.h"

Vec2 Wall::Update(Vec2 velocity, Vec2 shipposition)
{
	shipposition = CheckWall(shipposition, velocity, m_wallshape[0], m_wallshape[1]);
	shipposition = CheckWall(shipposition, velocity, m_wallshape[1], m_wallshape[2]);

	return shipposition;
}

Vec2 Wall::CheckWall(Vec2 &shipposition, Vec2 &velocity, Vec2 &wallpoint, Vec2 &wallpoint2)
{
	Vec2 wall = wallpoint - wallpoint2;
	Vec2 normVec = wall.PerpCCW().Normalize();

	Vec2 distanceVec = wallpoint - shipposition;
	Vec2 distanceShip = distanceVec.Proj(normVec);
	float distance = distanceShip.Length();
	if (distance <= 1.0f)
	{
		shipposition = shipposition + (normVec * 2.0f * (-velocity.Dot(normVec)));
	}
	return shipposition;
}

void Wall::Initialize(Vec2 position) {
	m_wallposition = position;
}

void Wall::Draw(Core::Graphics & graphics, Wall & wall)
{
	m_wallshape[0] = Vec2{ (float)m_wallposition.x * 1/4, 0 };
	m_wallshape[1] = Vec2{ 1, (float)m_wallposition.y / 2 };
	m_wallshape[2] = Vec2{ (float)m_wallposition.x * 1/4, (float)m_wallposition.y * 2};


	graphics.SetColor(RGB(255, 0, 255));
	for (int i = 0; i < 2; i++) {
		Vec2 point1 = wall.m_wallshape[i] + wall.m_wallposition;
		Vec2 point2 = wall.m_wallshape[i + 1] + wall.m_wallposition;
		graphics.DrawLine(m_wallshape[i].x, m_wallshape[i].y, m_wallshape[i + 1].x, m_wallshape[i + 1].y);
	}
}

