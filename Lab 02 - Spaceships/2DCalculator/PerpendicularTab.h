#pragma once
class RenderUI;
class PerpendicularTab
{
public:
	bool m_isInitialized;
public:
	PerpendicularTab(RenderUI* pRenderUI);
	~PerpendicularTab();
	static bool Initialize(RenderUI* pRenderUI);
	static bool Shutdown();
};

