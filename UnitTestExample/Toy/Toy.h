#pragma once
class Toy
{
public:
	int Doubler(int value) const ; // doubles the fun
	int Tripler(int value) const ; // triples the fun
	int GetNewId() { return ++m_id; }
	int GetCurrentId() { return m_id; }

public:
	int m_id = 0;
};

