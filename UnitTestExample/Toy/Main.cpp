#include <iostream>
#include "Toy.h"

int main() {
	printf("Hello, Toys!\n");
	
	Toy toy;

	int i = 17;
	int j = toy.Doubler(i);
	int k = toy.Tripler(i);
	printf("Doubler(%d) = %d\n", i, j);
	printf("Tripler(%d) = %d\n", i, k);

	return 0;
}