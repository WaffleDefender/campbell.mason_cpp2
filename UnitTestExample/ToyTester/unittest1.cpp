#include "stdafx.h"
#include "CppUnitTest.h"
#include "..\\Toy\Toy.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ToyTester
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestDoubler)
		{
			Toy toy;

			int i = 77;
			int k = toy.Doubler(i);
			Assert::AreEqual(k, i * 2);
		}
		TEST_METHOD(TestTripler) {
			Toy toy;

			int i = 77;
			int k = toy.Tripler(i);
			Assert::AreEqual(k, i * 3);
		}
		TEST_METHOD(TestGetCurrentId) {
			Toy toy;

			int actualId = toy.m_id;
			int currId = toy.GetCurrentId();

			Assert::AreEqual(actualId, currId);
		}
		TEST_METHOD(TestGetNewId) {
			Toy toy;

			int currId = toy.GetCurrentId();
			int newId = toy.GetNewId();

			Assert::AreEqual(currId + 1, newId);
		}

	};
}