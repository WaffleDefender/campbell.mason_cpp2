#include "LerpTab.h"
#include "RenderUI.h"
#include "Vec2.h"
#include <assert.h>

namespace {
	Vec2 firstLerpVec;
	Vec2 secondLerpVec;
	Vec2 aMinBLerpVec;
	Vec2 aLerpVec;
	Vec2 bLerpVec;
	Vec2 resultLerpVec;

	void LerpTabCallback(const LerpData& data) {
		firstLerpVec = Vec2{ data.a_i, data.a_j };
		secondLerpVec = Vec2{ data.b_i, data.b_j };
		aMinBLerpVec = secondLerpVec - firstLerpVec;
		resultLerpVec = resultLerpVec.Lerp(firstLerpVec, secondLerpVec, data.beta);
		aLerpVec = resultLerpVec.Proj(firstLerpVec);
		bLerpVec = resultLerpVec.Rej(firstLerpVec);
	}
}

LerpTab::LerpTab(RenderUI * pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


LerpTab::~LerpTab()
{
	Shutdown();
}

bool LerpTab::Initialize(RenderUI * pRenderUI)
{
	assert(pRenderUI);
	pRenderUI->setLerpData(firstLerpVec.Pos(), secondLerpVec.Pos(), aMinBLerpVec.Pos(), aLerpVec.Pos(), bLerpVec.Pos(), resultLerpVec.Pos(), LerpTabCallback);
	return true;
}

bool LerpTab::Shutdown()
{
	return true;
}
