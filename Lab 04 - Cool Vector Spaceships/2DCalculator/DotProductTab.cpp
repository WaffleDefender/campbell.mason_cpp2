#include "DotProductTab.h"
#include "RenderUI.h"
#include "Vec2.h"
#include <assert.h>

namespace {
	Vec2 vectorOne;
	Vec2 vectorTwo;
	Vec2 projectionVec;
	Vec2 rejectionVec;

	void DotProductTabCallback(const DotProductData& data) {
		vectorOne = Vec2{ data.v1i, data.v1j };
		vectorTwo = Vec2{ data.v2i, data.v2j };
		projectionVec = (!data.projectOntoLeftVector) ? vectorOne.Proj(vectorTwo) : vectorTwo.Proj(vectorOne);
		rejectionVec = (!data.projectOntoLeftVector) ? vectorOne.Rej(vectorTwo) : vectorTwo.Rej(vectorOne);
	}
}

DotProductTab::DotProductTab(RenderUI* pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


DotProductTab::~DotProductTab()
{
	Shutdown();
}

bool DotProductTab::Initialize(RenderUI * pRenderUI)
{
	assert(pRenderUI);
	pRenderUI->setDotProductData(vectorOne.Pos(), vectorTwo.Pos(), projectionVec.Pos(), rejectionVec.Pos(), DotProductTabCallback);
	return true;
}

bool DotProductTab::Shutdown()
{
	return false;
}
