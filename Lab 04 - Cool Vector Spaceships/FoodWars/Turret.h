#pragma once
#include "Core.h"
#include "Vec3.h"
#include "Mat3.h"
#include "Keyboard.h"
#include "MissileManager.h"
class Turret
{
public:
	void Draw(Core::Graphics & graphics);
	void Update(Mat3 position);

private:
	MissileManager m_missiles;
	Vec3 m_shaft;
	Vec3 m_turret[6];
	Keyboard m_keyboard;
};

