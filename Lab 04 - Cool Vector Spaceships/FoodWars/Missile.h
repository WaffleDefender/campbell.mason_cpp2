#pragma once
#include "Core.h"
#include "Vec3.h"
class Missile
{
public:
	void Draw(Core::Graphics & graphics);
	void Update(Vec3 position, Vec3 direction);

private:
	Vec3 m_position;
	Vec3 m_direction;
	int m_lifespan = 5;
};

