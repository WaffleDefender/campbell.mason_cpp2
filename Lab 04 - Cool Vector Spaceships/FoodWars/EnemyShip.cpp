#include "EnemyShip.h"

EnemyShip::EnemyShip(Vec3 position)
{
	InitializePoints();
	InitializePath();
	printf("\n(4), 5 : "); printf((m_showpath) ? "Show Enemy Path" : "Hide Enemy Path");
}

bool EnemyShip::Update(float dt)
{
	m_timer += dt;
	if (m_side1And2 && m_side3And4)
	{
		m_enemyposition = m_enemyposition.Translation(m_enemypath[0].Lerp(m_enemypath[0], m_enemypath[1], GetPercentage()));
		if (GetPercentage() >= 1.0f) m_side1And2 = false;
	}
	else if (!m_side1And2 && m_side3And4)
	{
		m_enemyposition = m_enemyposition.Translation(m_enemypath[1].Lerp(m_enemypath[1], m_enemypath[2], GetPercentage()));
		if (GetPercentage() >= 1.0f) m_side3And4 = false;
	}
	else if (!m_side1And2 && !m_side3And4)
	{
		m_enemyposition = m_enemyposition.Translation(m_enemypath[2].Lerp(m_enemypath[2], m_enemypath[3], GetPercentage()));
		if (GetPercentage() >= 1.0f) m_side1And2 = true;
	}
	else if (m_side1And2 && !m_side3And4)
	{
		m_enemyposition = m_enemyposition.Translation(m_enemypath[3].Lerp(m_enemypath[3], m_enemypath[4], GetPercentage()));
		if (GetPercentage() >= 1.0f) m_side3And4 = true;
	}
	if (GetPercentage() >= 1.0f)
	{
		m_percent = 0.0f;
		m_timer = 0;
	}
	CheckPath();
	return false;
}


float EnemyShip::GetPercentage()
{
	float pointReached = (m_enemypath[1] - m_enemypath[0]).Length() / m_enemyspeed;
	m_percent = m_timer / pointReached;
	return m_percent;
}

void EnemyShip::Draw(Core::Graphics & graphics, EnemyShip & ship)
{
	graphics.SetColor(RGB(165, 65, 65));
	for (int i = 0; i < 8; ++i)
	{
		Vec3 point1 = ship.m_enemyposition * ship.m_enemyship[i];
		Vec3 point2 = ship.m_enemyposition * ship.m_enemyship[i + 1];
		ship.DrawLine(graphics, point1, point2);
	}
	DrawPath(graphics, ship);
}

void EnemyShip::DrawPath(Core::Graphics & graphics, EnemyShip & ship)
{
	if (m_showpath) {
		graphics.SetColor(RGB(165, 0, 0));

		for (int i = 0; i < 4; ++i)
		{
			Vec3 point1 = ship.m_enemypath[i];
			Vec3 point2 = ship.m_enemypath[i + 1];
			ship.DrawLine(graphics, point1, point2);
		}
	}
}

void EnemyShip::DrawLine(Core::Graphics & graphics, const Vec3 & p1, const Vec3 & p2)
{
	graphics.DrawLine(p1.x, p1.y, p2.x, p2.y);
}

void EnemyShip::InitializePoints()
{
	m_enemyposition = m_enemyposition.Translation(m_enemypath[0]);
	m_enemyship[0] = Vec3{ 10, 0, 1 };
	m_enemyship[1] = Vec3{ 8, -6, 1 };
	m_enemyship[2] = Vec3{ 6, -12, 1 };
	m_enemyship[3] = Vec3{ 4, -18, 1 };
	m_enemyship[4] = Vec3{ -4, -18, 1 };
	m_enemyship[5] = Vec3{ -6, -12, 1 };
	m_enemyship[6] = Vec3{ -8, -6, 1 };
	m_enemyship[7] = Vec3{ -10, 0, 1 };
	m_enemyship[8] = Vec3{ 10, 0, 1 };
}

void EnemyShip::InitializePath()
{
	m_enemypath[0] = Vec3{ 700, 200, 1 };
	m_enemypath[1] = Vec3{ 400, 200, 1 };
	m_enemypath[2] = Vec3{ 400, 300, 1 };
	m_enemypath[3] = Vec3{ 700, 400, 1 };
	m_enemypath[4] = Vec3{ 700, 200, 1 };
}

void EnemyShip::CheckPath()
{
	if (m_keyboard.KeyNumPadFourPressed() && !m_showpath) {
		m_showpath = true;
		printf("\n(4), 5 : "); printf((m_showpath) ? "Show Enemy Path" : "Hide Enemy Path");
	}
	else if (m_keyboard.KeyNumPadFivePressed() && m_showpath) {
		m_showpath = false;
		printf("\n4, (5) : "); printf((m_showpath) ? "Show Enemy Path" : "Hide Enemy Path");
	}
}
