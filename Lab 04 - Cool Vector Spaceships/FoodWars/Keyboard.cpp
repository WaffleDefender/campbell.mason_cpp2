#include "Keyboard.h"
#include "Core.h"
#include <conio.h>

Vec3 Keyboard::VelocityUp(float dt)
{
	return Vec3{ 0, -1, 0} * (dt * m_speed);
}

Vec3 Keyboard::VelocityDown(float dt)
{
	return Vec3{ 0, 1, 0 } *(dt * m_speed);
}

Vec3 Keyboard::VelocityRight(float dt)
{
	return Vec3{ 1, 0, 0 } *(dt * m_speed);
}

Vec3 Keyboard::VelocityLeft(float dt)
{
	return Vec3{ -1, 0, 0 } *(dt * m_speed);
}

bool Keyboard::KeyUpPressed()
{
	return (Core::Input::IsPressed(Core::Input::KEY_UP)) ? true : false;
}

bool Keyboard::KeyDownPressed()
{
	return (Core::Input::IsPressed(Core::Input::KEY_DOWN)) ? true : false;
}

bool Keyboard::KeyLeftPressed()
{
	return (Core::Input::IsPressed(Core::Input::KEY_LEFT)) ? true : false;
}

bool Keyboard::KeyRightPressed()
{
	return (Core::Input::IsPressed(Core::Input::KEY_RIGHT)) ? true : false;
}

bool Keyboard::KeyWPressed()
{
	return (Core::Input::IsPressed(Core::Input::BUTTON_W)) ? true : false;
}

bool Keyboard::KeyAPressed()
{
	return (Core::Input::IsPressed(Core::Input::BUTTON_A)) ? true : false;
}

bool Keyboard::KeySPressed()
{
	return (Core::Input::IsPressed(Core::Input::BUTTON_S)) ? true : false;
}

bool Keyboard::KeyDPressed()
{
	return (Core::Input::IsPressed(Core::Input::BUTTON_D)) ? true : false;
}

bool Keyboard::KeyPPressed()
{
	return (Core::Input::IsPressed(Core::Input::BUTTON_P)) ? true : false;
}

bool Keyboard::KeyNumPadOnePressed()
{
	return (Core::Input::IsPressed(VK_NUMPAD1)) ? true : false;
}

bool Keyboard::KeyNumPadTwoPressed()
{
	return (Core::Input::IsPressed(VK_NUMPAD2)) ? true : false;
}

bool Keyboard::KeyNumPadFourPressed()
{
	return (Core::Input::IsPressed(VK_NUMPAD4)) ? true : false;
}

bool Keyboard::KeyNumPadFivePressed()
{
	return (Core::Input::IsPressed(VK_NUMPAD5)) ? true : false;
}

bool Keyboard::KeyEscapePressed()
{
	return (Core::Input::IsPressed(Core::Input::KEY_ESCAPE)) ? true : false;
}

bool Keyboard::MouseOnePressed()
{
	return (Core::Input::IsPressed(Core::Input::BUTTON_LEFT)) ? true: false;
}
