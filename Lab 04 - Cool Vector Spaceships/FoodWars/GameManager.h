#pragma once
#include "Core.h"
#include "Spaceship.h"
#include "FPS.h"
class GameManager
{
public:
	GameManager(int width = 800, int height = 800);
	~GameManager();

	bool IsInitialized() const { return m_isInitialized; }
	bool RunGame();
	bool Update(float dt);
	void Draw(Core::Graphics& graphics);

public:
	static bool UpdateCallBack(float dt)
	{
		return m_manager->Update(dt);
	}
	static void DrawCallBack(Core::Graphics& graphics)
	{
		m_manager->Draw(graphics);
	}
	static int GetScreenWidth() {
		return m_manager->m_screenWidth;
	}
	static int GetScreenHeight() {
		return m_manager->m_screenHeight;
	}

public:
	int m_screenWidth;
	int m_screenHeight;

private:
	bool Initialize();
	bool Shutdown(); 

private:
	static GameManager* m_manager;
	Spaceship m_spaceship;
	FPS m_fps;
	EnemyShip m_enemyship;
	Wall m_wall;
	bool m_isInitialized{ false };
	bool m_isPaused = false;
};

