#include "MissileManager.h"

void MissileManager::Draw(Core::Graphics & graphics)
{
	graphics.SetColor(RGB(m_redVal, m_greenVal, m_blueVal));

	m_blueVal += 5;
	m_redVal += 5;
	m_greenVal -= 5;

	m_missile[0].Draw(graphics);
	m_missile[1].Draw(graphics);
	m_missile[2].Draw(graphics);
}

void MissileManager::Update(Vec3 position, Vec3 direction)
{
	if (m_keyboard.MouseOnePressed()) {
		Vec3 distance;
		distance = position - direction;

		m_missile[0].Update(position, direction);
		m_missile[1].Update(position + distance, direction);
		m_missile[2].Update(position + distance + distance, direction);
		
	}
}
 