#include "GameManager.h"
#include "Vec2.h"
#include "Core.h"
#include <iostream>

typedef unsigned long  RGB;
typedef unsigned char  BYTE;
typedef unsigned short WORD;
#define RGB(r,g,b) ((COLORREF)(((BYTE)(r)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(b))<<16)))

int main() {
	printf("Hello from FoodWars!");
	printf("\nMove your ship WASD keys!");
	Vec2 vec2;
	GameManager game(800, 800);
	if (!game.IsInitialized()) return -1;
	game.RunGame();

	return 0;
}