#pragma once
#include "Core.h"
class FPS
{
public:
	void CalcFPS(float dt);
	void DisplayFPS(Core::Graphics &graphics);

private:
	int m_fps;
};

