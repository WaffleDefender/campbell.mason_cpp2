#include "Wall.h"
#include "GameManager.h"

Vec3 Wall::Update(Vec3 shipposition)
{
	shipposition = CheckWall(shipposition, m_wallshape[0], m_wallshape[1]);

	return shipposition;
}

Vec3 Wall::CheckWall(Vec3 &shipposition, Vec3 &wallpoint, Vec3 &wallpoint2)
{
	Vec3 midpoint = Vec3{ (wallpoint.x + wallpoint2.x) / 2, (wallpoint.y + wallpoint2.y) / 2, 0 };

	Vec3 wall = wallpoint - wallpoint2;
	Vec3 normVec = wall.PerpCW().Normalize();
	Vec3 distanceVec = wallpoint - shipposition;
	Vec3 distanceShip = distanceVec.Proj(normVec);
	float distance = distanceShip.Length();
	if (distance <= 5.0f)
	{
		if (m_shipPos.x >= shipposition.x && m_shipPos.y >= shipposition.y ) {
			shipposition = shipposition + (normVec * 5.0f * (shipposition.Normalize().Dot(normVec)));
		}
		else {
			shipposition = shipposition - (normVec * 5.0f * (shipposition.Normalize().Dot(normVec)));
		}
	}
	m_distance = distance;
	m_shipPos = shipposition;
	return shipposition;
}

void Wall::Initialize(Vec3 position) {
	m_wallposition = position;

	m_wallshape[0] = Vec3{ (float)m_wallposition.x * 1 / 4, 1, 0 };
	m_wallshape[1] = Vec3{ 1, (float)m_wallposition.y * 1 / 4, 0 };
}

void Wall::Draw(Core::Graphics & graphics, Wall & wall)
{
	graphics.SetColor(RGB(255, 255, 255));
	for (int i = 0; i < 3; i++) {
		if (i != 1) {
			Vec3 point1 = wall.m_wallshape[i] + wall.m_wallposition;
			Vec3 point2 = wall.m_wallshape[i + 1] + wall.m_wallposition;

			graphics.DrawLine(m_wallshape[i].x, m_wallshape[i].y, m_wallshape[i + 1].x, m_wallshape[i + 1].y);
		}
	}
}

