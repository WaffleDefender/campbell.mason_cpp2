#pragma once
#include "Vec3.h"
#include "Core.h"
#include "Keyboard.h"
#include "Missile.h"
class MissileManager
{
public:
	void Draw(Core::Graphics & graphics);
	void Update(Vec3 position, Vec3 direction);
private:
	int m_blueVal = 0;
	int m_redVal = 0;
	int m_greenVal = 255;
	Keyboard m_keyboard;
	Missile m_missile[3];
};

