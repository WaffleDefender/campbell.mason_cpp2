#pragma once

#include "Core.h"
#include "Vec3.h"
#include "Keyboard.h"
#include "Turret.h"
#include "EnemyShip.h"
#include "Wall.h"
class Spaceship
{
public:
	Spaceship();
	bool Update(float dt);
	void Draw(Core::Graphics& graphics, Spaceship& ship);
	void DrawLine(Core::Graphics & graphics, const Vec3 & p1, const Vec3 & p2);
	void InitializePoints();

private:
	void CheckShipAction();
	void WrapShip();
	void BounceShip();
	void Rotation(float dt);
	void Translation();
	void DisplayAction(Core::Graphics& graphics);

private:
	Vec3 m_shipShape[11];
	Vec3 m_position;
	Mat3 m_modifier;
	Mat3 m_rotatemat;

	Keyboard m_keyboard;
	Wall m_wall;
	Turret m_turret;

	float m_dragCoefficient = 0.0f;
	float m_rotater = 0.0f;

	bool m_bounce = false;
	bool m_wrap = true;

	int m_blueVal = 0;
	int m_redVal = 0;
	int m_greenVal = 255;

};

