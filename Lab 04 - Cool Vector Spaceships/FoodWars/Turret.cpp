#include "Turret.h"

void Turret::Draw(Core::Graphics & graphics)
{
	graphics.SetColor(RGB(255, 255, 0));
	for (int i = 0; i < 5; i++) {
		if (i == 4) {
			graphics.SetColor(RGB(255, 0, 255));
			m_turret[0] = m_shaft;
			graphics.DrawLine(m_turret[0].x, m_turret[0].y, m_turret[5].x, m_turret[5].y);
		}
		else if (i+1 != 5) {
			graphics.DrawLine(m_turret[i].x, m_turret[i].y, m_turret[i + 1].x, m_turret[i + 1].y);
		}
	}
	m_missiles.Draw(graphics);
}

void Turret::Update(Mat3 position)
{
	m_shaft = Vec3{ position.topright, position.middleright, position.bottomright };

	m_turret[0] = position * Vec3{ 5, 5, 1 };
	m_turret[1] = position * Vec3{ 5, -5, 1 };
	m_turret[2] = position * Vec3{ -5, -5, 1 };
	m_turret[3] = position * Vec3{ -5, 5, 1 };
	m_turret[4] = position * Vec3{ 5, 5, 1 };
	m_turret[5] = ((Vec3{ (float)Core::Input::GetMouseX(), (float)Core::Input::GetMouseY(), 1 }) - m_shaft).Normalize() * 10.0f + m_shaft;

	m_missiles.Update(m_shaft, m_turret[5]);
}