#pragma once
#include "Core.h"
#include "Vec3.h"
class Wall
{
public:	
	Vec3 Update(Vec3 shipposition);
	Vec3 CheckWall(Vec3 &shipposition, Vec3 &wallpoint, Vec3 &wallpoint2);
	
	void Draw(Core::Graphics& graphics, Wall& wall);
	void Initialize(Vec3 position);

private:
	Vec3 m_wallshape[4];
	Vec3 m_wallposition;
	float m_distance = 0;
	float m_prevdistance = 0;
	Vec3 m_midpoint;
	Vec3 m_shipPos;
};

