#include "Missile.h"

void Missile::Draw(Core::Graphics & graphics)
{
	for (int i = 0; i < 5000; i++) {
		if (i == 5000 * 2 / 5 || i == 5000 * 3 / 5 || i == 5000 * 4 / 5 || i == 5000 * 5 / 5) {
			Vec3 distance;
			distance = m_position - m_direction;
			m_position = m_position + distance;
			m_direction = m_direction + distance;
		}
		graphics.DrawLine(m_position.x, m_position.y, m_direction.x, m_direction.y);
	}
}

void Missile::Update(Vec3 position, Vec3 direction)
{
	Vec3 distance;
	distance = position - direction;
	m_position = direction;
	m_direction = direction + distance;
}
