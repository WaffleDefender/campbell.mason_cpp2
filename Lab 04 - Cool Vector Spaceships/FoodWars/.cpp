#include "Shape.h"

Vec2 Shape::InitializeSpaceship()
{
	m_shipShape[0] = Vec2{ -10,0 };
	m_shipShape[1] = Vec2{ 0,-15 };
	m_shipShape[2] = Vec2{ 0,-15 };
	m_shipShape[3] = Vec2{ 10,0 };
	m_shipShape[4] = Vec2{ 10,0 };
	m_shipShape[5] = Vec2{ 0,15 };
	m_shipShape[6] = Vec2{ 0,15 };
	m_shipShape[7] = Vec2{ -10,0 };
	m_shipShape[8] = Vec2{ -15,15 };
	m_shipShape[9] = Vec2{ 15, 15 };
	m_shipShape[10] = m_shipShape[4];
	return *m_shipShape;
}
