#pragma once
#include "Mat3.h"
class Keyboard
{
public:
	Vec3 VelocityUp(float dt);
	Vec3 VelocityDown(float dt);
	Vec3 VelocityRight(float dt);
	Vec3 VelocityLeft(float dt);

	bool KeyUpPressed();
	bool KeyDownPressed();
	bool KeyLeftPressed();
	bool KeyRightPressed();
	bool KeyWPressed();
	bool KeyAPressed();
	bool KeySPressed();
	bool KeyDPressed();
	bool KeyPPressed();
	bool KeyNumPadOnePressed();
	bool KeyNumPadTwoPressed();
	bool KeyNumPadFourPressed();
	bool KeyNumPadFivePressed();
	bool KeyEscapePressed();
	bool MouseOnePressed();


private:
	float m_speed = 400.0f;
	char keyClicked = 'c';
};

