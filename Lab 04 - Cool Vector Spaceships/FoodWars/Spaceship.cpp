#include "Spaceship.h"
#include "GameManager.h"
#include <sstream>

Spaceship::Spaceship()
{
	m_position = { 400, 400, 1 };
	InitializePoints();
	m_wall.Initialize(Vec3(400, 400, 0));
	m_wrap = true;
	printf("\n(1), 2 : "); printf((m_wrap) ? "Wrap" : "Bounce");
	m_modifier = { 1, 0, m_position.x, 0, 1, m_position.y, 0, 0, m_position.z };
}

bool Spaceship::Update(float dt)	
{
	Rotation(dt);
	Translation();

	CheckShipAction();
	m_turret.Update(m_modifier);
	m_position = m_wall.Update(m_position);

	m_modifier = m_modifier.Translation(m_position) * m_rotatemat;
	return false;
}

void Spaceship::Draw(Core::Graphics & graphics, Spaceship & ship)
{
	graphics.SetColor(RGB(m_redVal, m_greenVal, m_blueVal));

	m_blueVal += 5;
	m_redVal += 5;
	m_greenVal -= 5;
	for (int i = 0; i < 10; ++i)
	{
		Vec3 point1 = m_modifier * ship.m_shipShape[i];
		Vec3 point2 = m_modifier * ship.m_shipShape[i + 1];
		ship.DrawLine(graphics, point1, point2);
	}
	m_turret.Draw(graphics);
	m_wall.Draw(graphics, m_wall);
	DisplayAction(graphics);
}

void Spaceship::DrawLine(Core::Graphics & graphics, const Vec3& p1, const Vec3 & p2)
{
	graphics.DrawLine(p1.x, p1.y, p2.x, p2.y);
}

void Spaceship::InitializePoints()
{
	m_shipShape[0] = Vec3{ -10, 1, 1 };
	m_shipShape[1] = Vec3{ 1, -15, 1 };
	m_shipShape[2] = Vec3{ 1, -15, 1 };
	m_shipShape[3] = Vec3{ 10, 1, 1 };
	m_shipShape[4] = Vec3{ 10, 1, 1 };
	m_shipShape[5] = Vec3{ 1, 15, 1 };
	m_shipShape[6] = Vec3{ 1, 15, 1 };
	m_shipShape[7] = Vec3{ -10, 1, 1 };
	m_shipShape[8] = Vec3{ -15, 15, 1 };
	m_shipShape[9] = Vec3{ 15, 15, 1 };
	m_shipShape[10] = m_shipShape[4];
}

void Spaceship::CheckShipAction()
{
	if (m_keyboard.KeyNumPadOnePressed() && m_bounce && !m_wrap) {
		m_bounce = false;
		m_wrap = true;
		printf("\n(1), 2 : "); printf((m_wrap) ? "Wrap" : "Bounce");
	}
	else if (m_keyboard.KeyNumPadTwoPressed() && !m_bounce && m_wrap) {
		m_bounce = true;
		m_wrap = false;
		printf("\n1, (2) : ");printf((m_bounce) ? "Bounce" : "Wrap");
	}
	if (m_wrap) {
		WrapShip();
	}
	else if(m_bounce){
		BounceShip();
	}
}
void Spaceship::DisplayAction(Core::Graphics & graphics)
{
	const int bufSize = 20;
	char timebuf[bufSize];
	if(m_wrap)
		sprintf_s(timebuf, bufSize, "Mode: Wrap");
	else
		sprintf_s(timebuf, bufSize, "Mode: Bounce");
	graphics.SetColor(RGB(0, 255, 255));
	graphics.DrawString(GameManager::GetScreenWidth() - 100, 12, timebuf);
}

void Spaceship::WrapShip()
{
	if (m_modifier.topright > GameManager::GetScreenWidth()) {
		m_position = Vec3{ 0, m_modifier.middleright, m_modifier.bottomright };
		m_modifier = m_modifier.Translation(m_position);
	}
	if (m_modifier.topright < 0) {
		m_position = Vec3{ (float)GameManager::GetScreenWidth(), m_modifier.middleright, m_modifier.bottomright };
		m_modifier = m_modifier.Translation(m_position);
	}
	if (m_modifier.middleright > GameManager::GetScreenHeight()) {
		m_position = Vec3{ m_modifier.topright, 1,  m_modifier.bottomright };
		m_modifier = m_modifier.Translation(m_position);
	}
	if (m_modifier.middleright < 0) {
		m_position = Vec3{ m_modifier.topright, (float)GameManager::GetScreenHeight(), m_modifier.bottomright };
		m_modifier = m_modifier.Translation(m_position);
	}
}

void Spaceship::BounceShip()
{
	if (m_modifier.topright > GameManager::GetScreenWidth()) {
		m_position = Vec3{ (float) GameManager::GetScreenWidth(), m_position.y, m_modifier.bottomright };
		m_modifier = m_modifier.Translation(m_position);
	}
	if (m_modifier.topright < 0) {
		m_position = Vec3{ 1, m_position.y, m_modifier.bottomright };
		m_modifier = m_modifier.Translation(m_position);
	}
	if (m_modifier.middleright > GameManager::GetScreenHeight()) {
		m_position = Vec3{ m_position.x, (float)GameManager::GetScreenHeight(), m_modifier.bottomright };
		m_modifier = m_modifier.Translation(m_position);
	}
	if (m_modifier.middleright < 0) {
		m_position = Vec3{ m_position.x, 1, m_modifier.bottomright };
		m_modifier = m_modifier.Translation(m_position);
	}
}

void Spaceship::Rotation(float dt) 
{
	if (m_keyboard.KeyAPressed() || m_keyboard.KeyLeftPressed()) {
		m_rotater -= 5.0f * dt;
		m_rotatemat = m_modifier.Rotate(m_rotater);
	}
	if (m_keyboard.KeyDPressed() || m_keyboard.KeyRightPressed()) {
		m_rotater += 5.0f * dt;
		m_rotatemat = m_modifier.Rotate(m_rotater);
	}
	
}

void Spaceship::Translation()
{
	if ((m_keyboard.KeyWPressed() || m_keyboard.KeyUpPressed()) && m_dragCoefficient < 2){
		m_dragCoefficient = m_dragCoefficient + .05f;
		m_position = m_position - (m_rotatemat * Vec3{ 0, m_modifier.middleright, 0 }.Normalize()) * m_dragCoefficient;
	}
	if ((m_keyboard.KeySPressed() || m_keyboard.KeyDownPressed()) && m_dragCoefficient > -.50) {
		m_dragCoefficient = m_dragCoefficient - .05f;
		m_position = m_position + (m_rotatemat * Vec3{ 0, m_modifier.middleright, 0 }.Normalize()) * m_dragCoefficient;
	}
	m_position = m_position - (m_rotatemat * Vec3{ 0, m_modifier.middleright, 0 }).Normalize() *m_dragCoefficient;
	if (m_dragCoefficient >= 0){
		m_dragCoefficient -= .01f;
	}
	else if (m_dragCoefficient <= 0) {
		m_dragCoefficient += .01f;
	}

}
