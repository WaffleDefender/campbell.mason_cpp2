#include "Vec3.h"

std::ostream & operator<<(std::ostream & os, Vec3 v)
{
	return v.Output(os);
}

std::ostream & Vec3::Output(std::ostream & os) const
{
	return os << "[" << x << ", " << y << ", " << z << "]";
}
