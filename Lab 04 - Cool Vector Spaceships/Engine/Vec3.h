#ifndef Vec3_H_
#define Vec3_H_

#include <ostream>
#include <ExportHeader.h>

// This is an immutable Vector 2D class
class Vec3
{
public:
	float x, y, z;

public:
	Vec3() : Vec3(0.0f, 0.0f, 0.0f) {}
	Vec3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {}
	Vec3 operator+ (const Vec3& right) const
	{
		return Vec3{ x + right.x, y + right.y, z + right.z };
	}
	Vec3 operator- (const Vec3& right) const
	{
		return Vec3{ x - right.x, y - right.y, z - right.z };
	}
	Vec3 operator- () const
	{
		return Vec3{ -x, -y, -z };
	}
	Vec3 operator* (const Vec3& right) const
	{
		return Vec3{ x * right.x, y * right.y, z * right.z };
	}
	Vec3 operator* (const float right) const
	{
		return Vec3{ x * right, y * right, z * right };
	}
	Vec3 operator/ (const float right) const
	{
		return Vec3{ x / right, y / right, z / right };
	}
	Vec3 Lerp(const Vec3 start, const Vec3 end, const float point) const
	{
		return Vec3{ start + (end - start)*point };
	}
	Vec3 Normalize() const
	{
		float length = Length();
		if (length == 0)
		{
			return Vec3(0, 0, 0);
		}
		else
		{
			return Vec3 { x / length, y / length, z/length };
		}
	}
	Vec3 PerpCW() const
	{
		return Vec3{ y, -x, 0 };
	}
	Vec3 PerpCCW() const
	{
		return Vec3{ -y, x, 0 };
	}
	Vec3 Proj(const Vec3& right) const
	{
		return right * (right.Dot(*this)) / (right.Dot(right));
	}
	Vec3 Rej(const Vec3& right) const
	{
		return Vec3{ *this - Proj(right) };
	}
	

	float Dot(const Vec3& right) const { return x * right.x + y * right.y + z * right.z; }

	float Length() const { return sqrt(LengthSquared()); }

	float LengthSquared() const { return x * x + y * y + z * z; }

	std::ostream& Output(std::ostream& os) const;
};

inline Vec3 operator* (float scale, const Vec3& v) { return v * scale; }

std::ostream& operator<<(std::ostream& os, Vec3 v);

#endif // ndef Vec3_H_
