#ifndef Vec3_H_
#define Vec3_H_

#include <ostream>
#include <ExportHeader.h>

// This is an immutable Vector 2D class
class Vec3
{
public:
	float x, y, z;

public:
	Vec3() : Vec3(0.0f, 0.0f, 0.0f) {}
	Vec3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {}
	Vec3 operator+ (const Vec3& right) const
	{
		return Vec3{ x + right.x, y + right.y, z + right.z };
	}
	Vec3 operator- (const Vec3& right) const
	{
		return Vec3{ x - right.x, y - right.y, z - right.z };
	}
	Vec3 operator- () const
	{
		return Vec3{ -x, -y, -z };
	}
	Vec3 operator* (const Vec3& right) const
	{
		return Vec3{ x * right.x, y * right.y, z * right.z };
	}
	Vec3 operator* (const float right) const
	{
		return Vec3{ x * right, y * right, z * right };
	}
	Vec3 operator/ (const float right) const
	{
		return Vec3{ x / right, y / right, z / right };
	}

	std::ostream& Output(std::ostream& os) const;
};

inline Vec3 operator* (float scale, const Vec3& v) { return v * scale; }

std::ostream& operator<<(std::ostream& os, Vec3 v);

#endif // ndef Vec3_H_
