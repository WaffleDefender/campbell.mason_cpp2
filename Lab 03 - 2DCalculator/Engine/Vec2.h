#ifndef VEC2_H_
#define VEC2_H_

#include <ostream>
#include <ExportHeader.h>

// This is an immutable Vector 2D class
class Vec2
{
public:
	float x, y;

public:
	Vec2() : Vec2(0.0f, 0.0f) {}
	Vec2(float xx, float yy) : x(xx), y(yy) {}
	Vec2 operator+ (const Vec2& right) const
	{
		return Vec2{ x + right.x, y + right.y };
	}
	Vec2 operator- (const Vec2& right) const
	{
		return Vec2{ x - right.x, y - right.y };
	}
	Vec2 operator- () const
	{
		return Vec2{ -x, -y };
	}
	Vec2 operator* (const Vec2& right) const
	{
		return Vec2{ x * right.x, y * right.y };
	}
	Vec2 operator* (const float right) const
	{
		return Vec2{ x * right, y * right };
	}
	Vec2 operator/ (const float right) const
	{
		return Vec2{ x / right, y / right };
	}
	Vec2 Lerp(const Vec2 start, const Vec2 end, const float point) const
	{
		return Vec2{ start + (end - start)*point };
	}
	Vec2 Normalize() const
	{
		float length = Length();
		if (length == 0)
		{
			return Vec2(0, 0);
		}
		else
		{
			return Vec2{ x / length, y / length };
		}
	}
	Vec2 PerpCW() const
	{
		return Vec2{ y, -x };
	}
	Vec2 PerpCCW() const
	{
		return Vec2{ -y, x };
	}
	Vec2 Proj(const Vec2& right) const
	{
		return right * (right.Dot(*this))/(right.Dot(right));
	}
	Vec2 Rej(const Vec2& right) const
	{
		return Vec2{ *this - Proj(right) };
	}

	float Dot(const Vec2& right) const { return x * right.x + y * right.y; }

	float* Pos() { return &x; };

	float Length() const { return sqrt(LengthSquared()); }
	
	float LengthSquared() const { return x * x + y * y; }
	
	float Cross(const Vec2& right) const { return  x * right.x - y * right.y; }	
	
	std::ostream& Output(std::ostream& os) const;
};

inline Vec2 operator* (float scale, const Vec2& v) { return v * scale; }

std::ostream& operator<<(std::ostream& os, Vec2 v);

#endif // ndef VEC2_H_
