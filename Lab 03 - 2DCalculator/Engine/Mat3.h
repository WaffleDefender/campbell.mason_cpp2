#pragma once
#include "Vec3.h"
class Mat3
{
public:
	Mat3() : topleft(1), topmiddle(0), topright(0), middleleft(0), middlemiddle(1), middleright(0), bottomleft(0), bottommiddle(0), bottomright(1){}
	Mat3(Vec3 vec1, Vec3 Vec3) : topleft(vec1.x), topmiddle(Vec3.x), topright(0), middleleft(vec1.y), middlemiddle(Vec3.y), middleright(0), bottomleft(0), bottommiddle(0), bottomright(1) {}
	Mat3(Vec3 vec1, Vec3 vec2, Vec3 vec3) : topleft(vec1.x), topmiddle(vec2.x), topright(vec3.x), middleleft(vec1.y), middlemiddle(vec2.y), middleright(vec3.y), bottomleft(vec1.z), bottommiddle(vec2.z), bottomright(vec3.z) {}
	Mat3(float tleft, float tmiddle, float tright, float mleft, float mmiddle, float mright, float bleft, float bmiddle, float bright)
		: topleft(tleft), topmiddle(tmiddle), topright(tright),
		middleleft(mleft), middlemiddle(mmiddle), middleright(mright),
		bottomleft(bleft), bottommiddle(bmiddle), bottomright(bright){}
	Mat3 operator*(const Mat3 mat) const {
		return Mat3(topleft * mat.topleft + topmiddle * mat.middleleft + topright * mat.bottomleft, topleft * mat.topmiddle + topmiddle * mat.middlemiddle + topright * mat.bottommiddle, topleft * mat.topright + topmiddle * mat.middleright + topright * mat.bottomright,
			middleleft * mat.topleft + middlemiddle * mat.middleleft + middleright * mat.bottomleft, middleleft * mat.topmiddle + middlemiddle * mat.middlemiddle + middleright * mat.bottommiddle, middleleft * mat.topright + middlemiddle * mat.middleright + middleright * mat.bottomright,
			bottomleft * mat.topleft + bottommiddle * mat.middleleft + bottomright * mat.bottomleft, bottomleft * mat.topmiddle + bottommiddle * mat.middlemiddle + bottomright * mat.bottommiddle, bottomleft * mat.topright + bottommiddle * mat.bottomright + middleright * mat.bottomright);
	}
	Vec3 operator*(const Vec3 vec) const {
		return Vec3(topleft * vec.x + topmiddle * vec.y + topright * vec.z, 
			middleleft * vec.x + middlemiddle * vec.y + middleright * vec.z,
			bottomleft * vec.x + bottommiddle * vec.y + bottomright * vec.z);
	}
	Vec3 Mult(Vec3 vec) {
		return Vec3(topleft * vec.x + topmiddle * vec.y + topright * vec.z,
			middleleft * vec.x + middlemiddle * vec.y + middleright * vec.z,
			bottomleft * vec.x + bottommiddle * vec.y + bottomright * vec.z);
	}
	
public:
	static Mat3 Rotate(float degrees) {
		return Mat3(cosf(degrees), -sinf(degrees), 0, sinf(degrees), cosf(degrees), 0, 0, 0, 1);
	}
	static Mat3 Scale(float scale) {
		return Mat3(scale, 0, 0, 0, scale, 0, 0, 0, 1);
	}
	static Mat3 Scale(float scaleX, float scaleY) {
		return Mat3(scaleX, 0, 0, 0, scaleY, 0, 0, 0, 1);
	}
	static Mat3 Scale(Vec3 vec) {
		return Mat3(vec.x, 0, 0, 0, vec.y, 0, 0, 0, 1);
	}
	static Mat3 ScaleX(float scale) {
		return Mat3(scale, 0, 0, 0, 1, 0, 0, 0, 1);
	}
	static Mat3 ScaleY(float scale) {
		return Mat3(1, 0, 0, 0, scale, 0, 0, 0, 1);
	}
	static Mat3 Translation(float x, float y) {
		return Mat3(1, 0, x, 0, 1, y, 0, 0, 1);	
	}
	static Mat3 Translation(Vec3 vec) {
		return Mat3(1, 0, vec.x, 0, 1, vec.y, 0, 0, vec.z);
	}


public:
	float topleft;
	float topmiddle;
	float topright;
	float middleleft;
	float middlemiddle;
	float middleright;
	float bottomleft;
	float bottommiddle;
	float bottomright;
};

