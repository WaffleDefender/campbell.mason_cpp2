#pragma once
#include "Vec2.h"
class Mat2
{
public:
	Mat2() : topleft(1), topright(0), bottomleft(0), bottomright(1) {}
	Mat2(Vec2 vec1, Vec2 vec2) : topleft(vec1.x), topright(vec2.x), bottomleft(vec1.y), bottomright(vec2.y) {}
	Mat2(float topleft, float topright, float bottomleft, float bottomright) : topleft(topleft), topright(topright), bottomleft(bottomleft), bottomright(bottomright) {}

	Mat2 Mat2::operator* (const Mat2 mat) const {
		return Mat2(topleft * mat.topleft + topright * mat.bottomleft, topleft * mat.topright + topright * mat.bottomright,
			bottomleft * mat.topleft + bottomright * mat.bottomleft, bottomleft * mat.topright + bottomright * mat.bottomright);
	}
	Mat2 Mat2::operator* (const float f) const {
		return Mat2(topleft * f, topright * f, 
			bottomleft * f, bottomright * f);
	}
	Vec2 Mat2::operator* (const Vec2 vec) const {
		return Vec2(topleft * vec.x + topright * vec.y,
			bottomleft * vec.x + bottomright * vec.y);
	}

public:
	static Mat2 Rotate(float degrees) {
		return Mat2(cosf(degrees), -sinf(degrees), sinf(degrees), cosf(degrees));
	}
	static Mat2 Scale(float scale) {
		return Mat2(scale, 0, 0, scale);
	}
	static Mat2 Scale(Vec2 vec) {
		return Mat2(vec.x, 0, 0, vec.y);
	}
	static Mat2 Scale(float scaleX, float scaleY) {
		return Mat2(scaleX, 0, 0, scaleY);
	}
	static Mat2 ScaleX(float scale) {
		return Mat2(scale, 0, 0, 0);
	}
	static Mat2 ScaleY(float scale) {
		return Mat2(0, 0, 0, scale);
	}

public:
	float topleft;
	float topright;
	float bottomleft;
	float bottomright;
};

