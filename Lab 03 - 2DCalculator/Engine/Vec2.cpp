#include "Vec2.h"

std::ostream & operator<<(std::ostream & os, Vec2 v)
{
	return v.Output(os);
}

std::ostream & Vec2::Output(std::ostream & os) const
{
	return os << "[" << x << ", " << y << "]";
}
