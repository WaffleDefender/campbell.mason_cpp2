#include "CppUnitTest.h"
#include "Mat2.h"
#include "Vec2.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Mat2Test
{
	TEST_CLASS(UnitTest1)
	{
	public:
		TEST_METHOD(TestMat2MultiplacationToMat2) {
			Mat2 actual = { 8, 6, 4, 2 };
			Mat2 expected = { 88, 60, 40, 28 };

			actual = actual * Mat2{ 8, 6, 4, 2 };

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);

			Mat2 secondExpected = { -296, -296, -136, -136 };
			actual = actual * Mat2{ -2, -2, -2, -2 };

			Assert::AreEqual(actual.topleft, secondExpected.topleft);
			Assert::AreEqual(actual.topright, secondExpected.topright);
			Assert::AreEqual(actual.bottomleft, secondExpected.bottomleft);
			Assert::AreEqual(actual.bottomright, secondExpected.bottomright);
		}
		TEST_METHOD(TestMat2MultiplacationToVec2) {
			Mat2 actual = { 8, 8, 8, 8 };
			Vec2 vecActual = { 8, 8 };
			Vec2 expected = { 128, 128 };

			vecActual = actual * vecActual;

			Assert::AreEqual(vecActual.x, expected.x);
			Assert::AreEqual(vecActual.y, expected.y);

			Vec2 secondExpected = { -2048, -2048 };
			vecActual = actual * -vecActual;

			Assert::AreEqual(vecActual.x, secondExpected.x);
			Assert::AreEqual(vecActual.y, secondExpected.y);
		}
		TEST_METHOD(TestMat2MultiplacationToFloat) {
			Mat2 actual = { 8, 8, 8, 8 };
			Mat2 expected = { 64, 64, 64, 64 };

			actual = actual * 8;

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);

			Mat2 secondExpected = { -512, -512, -512, -512 };
			actual = actual * -8;

			Assert::AreEqual(actual.topleft, secondExpected.topleft);
			Assert::AreEqual(actual.topright, secondExpected.topright);
			Assert::AreEqual(actual.bottomleft, secondExpected.bottomleft);
			Assert::AreEqual(actual.bottomright, secondExpected.bottomright);
		}
		TEST_METHOD(TestMat2Rotate) {
			float cosfloat = cosf(8);
			float negativesinfloat = -sinf(8);
			float sinfloat = sinf(8);

			Mat2 expected = Mat2(cosfloat, negativesinfloat, sinfloat, cosfloat);
			Mat2 actual = Mat2().Rotate(8);

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);
		}
		TEST_METHOD(TestMat2ScaleFloat) {
			Mat2 expected = Mat2(8, 0, 0, 8);
			Mat2 actual = Mat2().Scale(8);

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);
		}
		TEST_METHOD(TestMat2ScaleVec2) {
			Vec2 helperVec = { 8, 8 };
			Mat2 expected = Mat2(8, 0, 0, 8);
			Mat2 actual = Mat2().Scale(helperVec);

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);
		}
		TEST_METHOD(TestMat2ScaleFloats) {
			Mat2 expected = Mat2(8, 0, 0, 8);
			Mat2 actual = Mat2().Scale(8, 8);

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);
		}
		TEST_METHOD(TestMat2ScaleXFloat) {
			Mat2 expected = Mat2(8, 0, 0, 0);
			Mat2 actual = Mat2().ScaleX(8);

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);
		}
		TEST_METHOD(TestMat2ScaleYFloat) {
			Mat2 expected = Mat2(0, 0, 0, 8);
			Mat2 actual = Mat2().ScaleY(8);

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);
		}
	};
}