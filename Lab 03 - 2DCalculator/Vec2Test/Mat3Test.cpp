#include "CppUnitTest.h"
#include "Mat3.h"
#include "Vec2.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Mat3Test
{
	TEST_CLASS(UnitTest1)
	{
	public:
		TEST_METHOD(TestMat3MultiplacationToMat3) {
			Mat3 actual = { 8, 6, 4, 2, 2, 4, 6, 8, 6 };
			Mat3 expected = { 100, 92, 80, 44, 48, 40, 100, 100, 96 };

			actual = actual * Mat3{ 8, 6, 4, 2, 2, 4, 6, 8, 6 };

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topmiddle, expected.topmiddle);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.middleleft, expected.middleleft);
			Assert::AreEqual(actual.middlemiddle, expected.middlemiddle);
			Assert::AreEqual(actual.middleright, expected.middleright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);

			Mat3 secondExpected = { -544, -544, -544, -264, -264, -264, -592, -592, -480 };
			actual = actual * Mat3{ -2, -2, -2, -2, -2, -2, -2, -2, -2 };

			Assert::AreEqual(actual.topleft, secondExpected.topleft);
			Assert::AreEqual(actual.topmiddle, secondExpected.topmiddle);
			Assert::AreEqual(actual.topright, secondExpected.topright);
			Assert::AreEqual(actual.middleleft, secondExpected.middleleft);
			Assert::AreEqual(actual.middlemiddle, secondExpected.middlemiddle);
			Assert::AreEqual(actual.middleright, secondExpected.middleright);
			Assert::AreEqual(actual.bottomleft, secondExpected.bottomleft);
			Assert::AreEqual(actual.bottomleft, secondExpected.bottomleft);
			Assert::AreEqual(actual.bottomright, secondExpected.bottomright);
		}
		TEST_METHOD(TestMat3MultiplacationToVec3) {
			Mat3 actual = { 8, 6, 4, 2, 2, 4, 6, 8, 6 };
			Vec3 actualVec = { 8, 6, 4 };
			Vec3 expected = { 116, 44, 120 };

			actualVec = actual * actualVec;

			Assert::AreEqual(actualVec.x, expected.x);
			Assert::AreEqual(actualVec.y, expected.y);
			Assert::AreEqual(actualVec.z, expected.z);

			Vec3 secondExpected = { -36, -16, -40 };
			actualVec = actual * Vec3{ -2, -2, -2};

			Assert::AreEqual(actualVec.x, secondExpected.x);
			Assert::AreEqual(actualVec.y, secondExpected.y);
			Assert::AreEqual(actualVec.z, secondExpected.z);
		}
		TEST_METHOD(TestMat3Rotate) {
			float cosfloat = cosf(8);
			float negativesinfloat = -sinf(8);
			float sinfloat = sinf(8);

			Mat3 expected = Mat3(cosfloat, negativesinfloat, 0, sinfloat, cosfloat, 0, 0, 0, 1);
			Mat3 actual = Mat3().Rotate(8);

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topmiddle, expected.topmiddle);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.middleleft, expected.middleleft);
			Assert::AreEqual(actual.middlemiddle, expected.middlemiddle);
			Assert::AreEqual(actual.middleright, expected.middleright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);
		}
		TEST_METHOD(TestMat3Scale) {
			Mat3 expected = Mat3(8, 0, 0, 0, 8, 0, 0, 0, 1);
			Mat3 actual = Mat3().Scale(8);

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topmiddle, expected.topmiddle);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.middleleft, expected.middleleft);
			Assert::AreEqual(actual.middlemiddle, expected.middlemiddle);
			Assert::AreEqual(actual.middleright, expected.middleright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);
		}
		TEST_METHOD(TestMat3ScaleFloats) {
			Mat3 expected = Mat3(8, 0, 0, 0, 4, 0, 0, 0, 1);
			Mat3 actual = Mat3().Scale(8, 4);

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topmiddle, expected.topmiddle);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.middleleft, expected.middleleft);
			Assert::AreEqual(actual.middlemiddle, expected.middlemiddle);
			Assert::AreEqual(actual.middleright, expected.middleright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);
		}
		TEST_METHOD(TestMat3ScaleVec3) {
			Vec3 helperVec = { 8, 8, 8 };
			Mat3 expected = Mat3(8, 0, 0, 0, 8, 0, 0, 0, 1);
			Mat3 actual = Mat3().Scale(helperVec);

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topmiddle, expected.topmiddle);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.middleleft, expected.middleleft);
			Assert::AreEqual(actual.middlemiddle, expected.middlemiddle);
			Assert::AreEqual(actual.middleright, expected.middleright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);
		}
		TEST_METHOD(TestMat3ScaleX) {
			Mat3 expected = Mat3(8, 0, 0, 0, 1, 0, 0, 0, 1);
			Mat3 actual = Mat3().ScaleX(8);

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topmiddle, expected.topmiddle);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.middleleft, expected.middleleft);
			Assert::AreEqual(actual.middlemiddle, expected.middlemiddle);
			Assert::AreEqual(actual.middleright, expected.middleright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);
		}
		TEST_METHOD(TestMat3ScaleY) {
			Mat3 expected = Mat3(1, 0, 0, 0, 8, 0, 0, 0, 1);
			Mat3 actual = Mat3().ScaleY(8);

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topmiddle, expected.topmiddle);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.middleleft, expected.middleleft);
			Assert::AreEqual(actual.middlemiddle, expected.middlemiddle);
			Assert::AreEqual(actual.middleright, expected.middleright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);
		}
		TEST_METHOD(TestMat3TranslationFloats) {
			Mat3 expected = Mat3(1, 0, 8, 0, 1, 4, 0, 0, 1);
			Mat3 actual = Mat3().Translation(8, 4);

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topmiddle, expected.topmiddle);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.middleleft, expected.middleleft);
			Assert::AreEqual(actual.middlemiddle, expected.middlemiddle);
			Assert::AreEqual(actual.middleright, expected.middleright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);
		}
		TEST_METHOD(TestMat3TranslationVec3) {
			Vec3 helperVec = { 8, 8, 8 };
			Mat3 expected = Mat3(1, 0, 8, 0, 1, 8, 0, 0, 8);
			Mat3 actual = Mat3().Translation(helperVec);

			Assert::AreEqual(actual.topleft, expected.topleft);
			Assert::AreEqual(actual.topmiddle, expected.topmiddle);
			Assert::AreEqual(actual.topright, expected.topright);
			Assert::AreEqual(actual.middleleft, expected.middleleft);
			Assert::AreEqual(actual.middlemiddle, expected.middlemiddle);
			Assert::AreEqual(actual.middleright, expected.middleright);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomleft, expected.bottomleft);
			Assert::AreEqual(actual.bottomright, expected.bottomright);
		}
	};
}