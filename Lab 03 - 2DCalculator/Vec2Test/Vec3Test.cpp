#include "CppUnitTest.h"
#include "Vec3.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Vec3Test
{
	TEST_CLASS(UnitTest1)
	{
	public:
		TEST_METHOD(TestVec3Addition)
		{
			Vec3 leftVec = { 1, 1, 1 };
			Vec3 rightVec = { 1, 1, 1 };
			Vec3 additionVec = { 1, 1, 1 };
			for (int i = 0; i < 5; i++) {
				leftVec = leftVec + rightVec;
				rightVec = rightVec + additionVec;
			}
			Assert::AreEqual(leftVec.x, (additionVec + Vec3{ 15, 15, 15 }).x);
			Assert::AreEqual(leftVec.y, (additionVec + Vec3{ 15, 15, 15 }).y);
		}
		TEST_METHOD(TestVec3Subtraction)
		{
			Vec3 leftVec = { 1, 1, 1 };
			Vec3 rightVec = { 1, 1, 1 };
			Vec3 subtractionVec = { -1, -1, -1 };
			for (int i = 0; i < 5; i++) {
				leftVec = (rightVec.x > 0) ? leftVec - rightVec : leftVec + rightVec;
				rightVec = rightVec + subtractionVec;
			}
			Assert::AreEqual(leftVec.x, (subtractionVec - Vec3{ 5, 5, 5 }).x);
			Assert::AreEqual(leftVec.y, (subtractionVec - Vec3{ 5, 5, 5 }).y);
		}
		TEST_METHOD(TestVec3Negation) {
			Vec3 actual = { 8, 8, 8 };
			Vec3 expected = { -8, -8, -8 };

			Assert::AreNotEqual(actual.x, expected.x);
			Assert::AreNotEqual(actual.y, expected.y);

			Vec3 secondActual = -actual;

			Assert::AreEqual(secondActual.x, expected.x);
			Assert::AreEqual(secondActual.y, expected.y);
		}
		TEST_METHOD(TestVec3MultiplacationToVec3) {
			Vec3 actual = { 8, 6, 4 };
			Vec3 expected = { 64, 36, 16 };

			actual = actual * Vec3{ 8, 6, 4 };

			Assert::AreEqual(actual.x, expected.x);
			Assert::AreEqual(actual.y, expected.y);

			Vec3 secondExpected = { -128, -72, -32 };
			actual = expected * Vec3{ -2, -2, -2 };

			Assert::AreEqual(actual.x, secondExpected.x);
			Assert::AreEqual(actual.y, secondExpected.y);
		}
		TEST_METHOD(TestVec3MultiplacationToFloat) {
			Vec3 actual = { 8, 8, 8 };
			Vec3 expected = { 64, 64, 64 };

			actual = actual * 8;

			Assert::AreEqual(actual.x, expected.x);
			Assert::AreEqual(actual.y, expected.y);

			Vec3 secondExpected = { -128, -128, -128 };
			actual = expected * -2;

			Assert::AreEqual(actual.x, secondExpected.x);
			Assert::AreEqual(actual.y, secondExpected.y);
		}
		TEST_METHOD(TestVec3Division) {
			Vec3 actual = { 64, 64, 64 };
			Vec3 expected = { 8, 8, 8 };

			actual = actual / 8;

			Assert::AreEqual(actual.x, expected.x);
			Assert::AreEqual(actual.y, expected.y);

			Vec3 secondExpected = { -4, -4, -4 };
			actual = expected / -2;

			Assert::AreEqual(actual.x, secondExpected.x);
			Assert::AreEqual(actual.y, secondExpected.y);
		}
	};
}