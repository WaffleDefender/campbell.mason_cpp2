#pragma once
#include "RenderUI.h"
class MatrixMultiplicationTab
{
public:
	bool m_isInitialized;
public:
	MatrixMultiplicationTab(RenderUI* pRenderUI);
	~MatrixMultiplicationTab();
	static bool Initialize(RenderUI* pRenderUI);
	static bool Shutdown();
};

