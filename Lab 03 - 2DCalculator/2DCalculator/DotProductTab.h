#pragma once
class RenderUI;
class DotProductTab
{
public:
	DotProductTab(RenderUI* pRenderUI);
	~DotProductTab();	
	static bool Initialize(RenderUI* pRenderUI);
	static bool Shutdown();

public:
	bool m_isInitialized;
};

