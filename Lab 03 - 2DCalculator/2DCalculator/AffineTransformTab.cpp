#include "AffineTransformTab.h"
#include "Vec3.h"
#include "Mat3.h"
#include <assert.h>


namespace {
	Vec3 vec1;
	Vec3 vec2;
	Vec3 vec3;
	Vec3 vec4;
	Vec3 vec5;
	Mat3 mat;
	Vec3 resultvectors[5];
	void AffineTransformationTabCallback(const AffineTransformationData& data) {
		mat = { data.data[0], data.data[1], data.data[2],
			data.data[3], data.data[4], data.data[5],
			data.data[6], data.data[7], data.data[8] };
		vec1 = { data.data[9], data.data[10], data.data[11] };
		vec2 = { data.data[12], data.data[13], data.data[14] };
		vec3 = { data.data[15], data.data[16], data.data[17] };
		vec4 = { data.data[18], data.data[19], data.data[20] };
		vec5 = { data.data[21], data.data[22], data.data[23] };

		resultvectors[0] = mat * vec1;
		resultvectors[1] = mat * vec2;
		resultvectors[2] = mat * vec3;
		resultvectors[3] = mat * vec4;
		resultvectors[4] = mat * vec5;
	}
}

AffineTransformTab::AffineTransformTab(RenderUI * pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}

AffineTransformTab::~AffineTransformTab()
{
	Shutdown();
	}

bool AffineTransformTab::Initialize(RenderUI * pRenderUI)
{
	assert(pRenderUI);
	pRenderUI->setAffineTransformationData(&resultvectors[0].x, AffineTransformationTabCallback);
	return true;
}

bool AffineTransformTab::Shutdown()
{
	return true;
}
