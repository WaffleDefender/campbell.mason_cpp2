#include "TabManager.h"
#include "AdditionTab.h"
#include "PerpendicularTab.h"
#include "DotProductTab.h"
#include "LerpTab.h"
#include "MatrixMultiplicationTab.h"
#include "AffineTransformTab.h"
#include "MatrixTransformTab.h"

bool TabManager::Initialize(RenderUI * renderUI)
{
	AdditionTab::AdditionTab(renderUI);
	PerpendicularTab::PerpendicularTab(renderUI);
	DotProductTab::DotProductTab(renderUI);
	LerpTab::LerpTab(renderUI);
	MatrixMultiplicationTab::MatrixMultiplicationTab(renderUI);
	AffineTransformTab::AffineTransformTab(renderUI);
	MatrixTransformTab::MatrixTransformTab(renderUI);

	return true;
}

bool TabManager::Shutdown()
{
	AdditionTab::Shutdown();
	PerpendicularTab::Shutdown();
	DotProductTab::Shutdown();
	LerpTab::Shutdown();
	MatrixMultiplicationTab::Shutdown();
	AffineTransformTab::Shutdown();
	MatrixTransformTab::Shutdown();

	return true;
}
