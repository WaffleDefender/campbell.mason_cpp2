#include <iostream>
#include "Engine.h"
#include "RenderUI.h"
#include "TabManager.h"


int main(int argc,  char** argv) {
	RenderUI renderUI;
	if (!TabManager::Initialize(&renderUI)) return -1;
	if (!renderUI.initialize(argc, argv)) return -1;

	int runResult = renderUI.run();

	renderUI.shutdown();

	return runResult;
}