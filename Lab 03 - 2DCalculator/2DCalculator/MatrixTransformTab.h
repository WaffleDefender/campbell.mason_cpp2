#pragma once
#include "RenderUI.h"
class MatrixTransformTab
{
public:
	bool m_isInitialized;
public:
	MatrixTransformTab(RenderUI* pRenderUI);
	~MatrixTransformTab();
	static bool Initialize(RenderUI* pRenderUI);
	static bool Shutdown();
};

