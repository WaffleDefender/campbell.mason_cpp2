#pragma once
#include "RenderUI.h"
class AffineTransformTab
{
public:
	bool m_isInitialized;
public:
	AffineTransformTab(RenderUI* pRenderUI);
	~AffineTransformTab();
	static bool Initialize(RenderUI* pRenderUI);
	static bool Shutdown();
};

