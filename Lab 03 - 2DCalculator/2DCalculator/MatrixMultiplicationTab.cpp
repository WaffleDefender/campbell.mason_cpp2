#include "MatrixMultiplicationTab.h"
#include <assert.h>
#include "Vec2.h"
#include "Mat2.h"

namespace {
	Vec2 vec;
	Mat2 mat;
	Vec2 resVec;

	void MatrixMultiplicationTabCallback(const LinearTransformationData& data) {
		mat = { data.m00, data.m01, data.m10, data.m11 };
		vec = { data.v0, data.v1 };	
		resVec = mat * vec;
	}
}

MatrixMultiplicationTab::MatrixMultiplicationTab(RenderUI *pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


MatrixMultiplicationTab::~MatrixMultiplicationTab()
{
	Shutdown();
}

bool MatrixMultiplicationTab::Initialize(RenderUI * pRenderUI)
{
	assert(pRenderUI);
	pRenderUI->setLinearTransformationData(&resVec.x, MatrixMultiplicationTabCallback);
	return true;
}

bool MatrixMultiplicationTab::Shutdown()	
{
	return true;
}
