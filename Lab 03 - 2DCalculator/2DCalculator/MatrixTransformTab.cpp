#include "MatrixTransformTab.h"
#include "Mat3.h"
#include "Vec2.h"
#include <assert.h>

namespace {
	Mat3 mat;
	Vec2 shipshape[8];
	Mat3 objects[20];
	void MatrixTransformTabCallback(const MatrixTransformData2D& data) 
	{
		mat = Mat3(); 

		shipshape[0] = { .10f, .10f };
		shipshape[1] = { .10f, -.10f };
		shipshape[2] = { .10f, -.10f };
		shipshape[3] = { -.10f, -.10f };
		shipshape[4] = { -.10f, -.10f };
		shipshape[5] = { -.10f, .10f };
		shipshape[6] = { -.10f, .10f };		
		shipshape[7] = { .10f, .10f };

		objects[data.selectedMatrix] = mat.Rotate(data.rotate) * mat.Scale(data.scaleX, data.scaleY) * mat.Translation(data.translateX, data.translateY);
	}
}

MatrixTransformTab::MatrixTransformTab(RenderUI * pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}

MatrixTransformTab::~MatrixTransformTab()
{
	Shutdown();
}

bool MatrixTransformTab::Initialize(RenderUI * pRenderUI)
{
	assert(pRenderUI);
	pRenderUI->set2DMatrixVerticesTransformData(&shipshape->x, 8, &objects->topleft, &objects[0].topleft, MatrixTransformTabCallback);
	return true;
}

bool MatrixTransformTab::Shutdown()
{
	return true;
}
