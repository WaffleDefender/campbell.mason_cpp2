#include "AdditionTab.h"
#include "RenderUI.h"
#include "Vec2.h"
#include <assert.h>

namespace {
	Vec2 left;
	Vec2 right;
	Vec2 result;

	void AdditionTabCallback(const BasicVectorEquationInfo& data){

		left = data.scalar1 * Vec2(data.x1, data.y1);
		right = data.scalar2 * Vec2(data.x2, data.y2);
		result = data.add ? left + right : left - right;

	}
}

AdditionTab::AdditionTab(RenderUI *pRenderUI) :  m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


AdditionTab::~AdditionTab()
{
	Shutdown();
}

bool AdditionTab::Initialize(RenderUI * pRenderUI)
{
	assert(pRenderUI);
	pRenderUI->setBasicVectorEquationData(AdditionTabCallback, left.Pos(), right.Pos(), result.Pos());
	return true;
}
	
bool AdditionTab::Shutdown()
{
	return true;
}
