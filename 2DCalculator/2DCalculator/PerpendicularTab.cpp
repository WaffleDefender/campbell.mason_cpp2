#include "PerpendicularTab.h"
#include "RenderUI.h"
#include "Vec2.h"
#include <assert.h>


namespace {
	Vec2 originalVec{ 1, 1 };
	Vec2 normalVec{ 1, 1 };
	Vec2 clockwiseVec{ 1, 1 };
	Vec2 counterclockwiseVec{ 1, 1 };

	void PerpendicularTabCallback(const PerpendicularData& data) {

		originalVec = Vec2{ data.x, data.y };
		normalVec = originalVec.Normalize();
		clockwiseVec = originalVec.PerpCW();
		counterclockwiseVec = originalVec.PerpCCW();
	}
}

PerpendicularTab::PerpendicularTab(RenderUI * pRenderUI) : m_isInitialized(false)
{
	m_isInitialized = Initialize(pRenderUI);
}


PerpendicularTab::~PerpendicularTab()
{
	Shutdown();
}

bool PerpendicularTab::Initialize(RenderUI * pRenderUI)
{
	assert(pRenderUI);
	pRenderUI->setPerpendicularData(originalVec.Pos(), normalVec.Pos(), clockwiseVec.Pos(), counterclockwiseVec.Pos(), PerpendicularTabCallback);
	return true;
}

bool PerpendicularTab::Shutdown()
{
	return true;
}
