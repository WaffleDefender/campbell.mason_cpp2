#include "TabManager.h"
#include "AdditionTab.h"
#include "PerpendicularTab.h"
#include "DotProductTab.h"
#include "LerpTab.h"

bool TabManager::Initialize(RenderUI * renderUI)
{
	AdditionTab::AdditionTab(renderUI);
	PerpendicularTab::PerpendicularTab(renderUI);
	DotProductTab::DotProductTab(renderUI);
	LerpTab::LerpTab(renderUI);

	return true;
}

bool TabManager::Shutdown()
{
	AdditionTab::Shutdown();
	PerpendicularTab::Shutdown();
	DotProductTab::Shutdown();
	LerpTab::Shutdown();

	return true;
}
