#include "CppUnitTest.h"
#include "Vec2.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Vec2Test
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		TEST_METHOD(TestVec2Addition)
		{
			Vec2 leftVec = { 1, 1 };
			Vec2 rightVec = { 1, 1 };
			Vec2 additionVec = { 1, 1 };
			for (int i = 0; i < 5; i++) {
				leftVec = leftVec + rightVec;
				rightVec = rightVec + additionVec;
			}		
			Assert::AreEqual(leftVec.x, (additionVec + Vec2{ 15, 15 }).x);
			Assert::AreEqual(leftVec.y, (additionVec + Vec2{ 15, 15 }).y);
		}
		TEST_METHOD(TestVec2Subtraction)
		{
			Vec2 leftVec = { 1, 1 };
			Vec2 rightVec = { 1, 1 };
			Vec2 subtractionVec = { -1, -1 };
			for (int i = 0; i < 5; i++) {
				leftVec = (rightVec.x > 0) ? leftVec - rightVec : leftVec + rightVec;
				rightVec = rightVec + subtractionVec;
			}
			Assert::AreEqual(leftVec.x, (subtractionVec - Vec2{ 5, 5 }).x);
			Assert::AreEqual(leftVec.y, (subtractionVec - Vec2{ 5, 5 }).y);
		}
		TEST_METHOD(TestVec2Negation) {
			Vec2 actual = { 8, 8 };
			Vec2 expected = { -8, -8 };

			Assert::AreNotEqual(actual.x, expected.x);
			Assert::AreNotEqual(actual.y, expected.y);

			Vec2 secondActual = -actual;

			Assert::AreEqual(secondActual.x, expected.x);
			Assert::AreEqual(secondActual.y, expected.y);
		}
		TEST_METHOD(TestVec2MultiplacationToVec2) {
			Vec2 actual = { 8, 6 };
			Vec2 expected = { 64, 36 };

			actual = actual * Vec2{ 8, 6 };

			Assert::AreEqual(actual.x, expected.x);
			Assert::AreEqual(actual.y, expected.y);

			Vec2 secondExpected = { -128, -72 };
			actual = expected * Vec2{ -2, -2 };

			Assert::AreEqual(actual.x, secondExpected.x);
			Assert::AreEqual(actual.y, secondExpected.y);
		}
		TEST_METHOD(TestVec2MultiplacationToFloat) {
			Vec2 actual = { 8, 8 };
			Vec2 expected = { 64, 64 };

			actual = actual * 8;

			Assert::AreEqual(actual.x, expected.x);
			Assert::AreEqual(actual.y, expected.y);

			Vec2 secondExpected = { -128, -128 };
			actual = expected * -2;

			Assert::AreEqual(actual.x, secondExpected.x);
			Assert::AreEqual(actual.y, secondExpected.y);
		}
		TEST_METHOD(TestVec2Division) {
			Vec2 actual = { 64, 64 };
			Vec2 expected = { 8, 8 };

			actual = actual / 8;

			Assert::AreEqual(actual.x, expected.x);
			Assert::AreEqual(actual.y, expected.y);

			Vec2 secondExpected = { -4, -4 };
			actual = expected / -2;

			Assert::AreEqual(actual.x, secondExpected.x);
			Assert::AreEqual(actual.y, secondExpected.y);
		}
		TEST_METHOD(TestVec2Lerp) {
			Vec2 firstLerpVec = Vec2 { -6, 5 };
			Vec2 secondLerpVec = Vec2 { 5, 8.35000038f };
			Vec2 actualResultLerpVec;
			Vec2 expectedResultLerpVec = { -1.59999990f, 6.34000015f };

			actualResultLerpVec = actualResultLerpVec.Lerp(firstLerpVec, secondLerpVec, 0.400000006f);

			Assert::AreEqual(actualResultLerpVec.x, expectedResultLerpVec.x);
			Assert::AreEqual(actualResultLerpVec.y, expectedResultLerpVec.y);
		}
		TEST_METHOD(TestVec2Normalize) {
			Vec2 actual;
			Vec2 modifier = { 1, 1 };
			Vec2 expected = { 0.707106769f, 0.707106769f };

			for (int i = 0; i < 5; i++) {
				actual = (actual + modifier).Normalize();

				Assert::AreEqual(actual.x, expected.x);
				Assert::AreEqual(actual.y, expected.y);
			}
		}
		TEST_METHOD(TestVec2PerpCW) {
			Vec2 actual = { 4, 8 };
			Vec2 expected = { 8, -4 };

			actual = actual.PerpCW();

			Assert::AreEqual(actual.x, expected.x);
			Assert::AreEqual(actual.y, expected.y);
		}
		TEST_METHOD(TestVec2PerpCCW) {
			Vec2 actual = { 4, 8 };
			Vec2 expected = { -8, 4 };

			actual = actual.PerpCCW();

			Assert::AreEqual(actual.x, expected.x);
			Assert::AreEqual(actual.y, expected.y);
		}
		TEST_METHOD(TestVec2Proj) {
			Vec2 vectorOne = Vec2{ 7, 1 };
			Vec2 vectorTwo = Vec2{ 3, 6 };
			Vec2 actual = vectorTwo.Proj(vectorOne);
			Vec2 expected = Vec2{ 3.77999997f, 0.540000021f };

			Assert::AreEqual(actual.x, expected.x);
			Assert::AreEqual(actual.y, expected.y);

			expected = Vec2{ 1.79999995f, 3.59999990f };
			actual = vectorOne.Proj(vectorTwo);;

			Assert::AreEqual(actual.x, expected.x);
			Assert::AreEqual(actual.y, expected.y);
		}
		TEST_METHOD(TestVec2Rej) {
			Vec2 vectorOne = Vec2{ 7, 1 };
			Vec2 vectorTwo = Vec2{ 3, 6 };
			Vec2 actual = vectorTwo.Rej(vectorOne);
			Vec2 expected = Vec2{ -0.779999971f, 5.46000004f };

			Assert::AreEqual(actual.x, expected.x);
			Assert::AreEqual(actual.y, expected.y);

			expected = Vec2{ 5.19999981f, -2.59999990f };
			actual = vectorOne.Rej(vectorTwo);;

			Assert::AreEqual(actual.x, expected.x);
			Assert::AreEqual(actual.y, expected.y);
		}
		TEST_METHOD(TestVec2Length) {
			Vec2 actual = { 4, 4 };
			float actualLength = 0;
			float expected = 5.65685415f;

			actualLength = actual.Length();

			Assert::AreEqual(actualLength, expected);
		}
		TEST_METHOD(TestVec2LengthSquared) {
			Vec2 actual = { 4, 4 };
			float actualLength = 0;
			float expected = 32;

			actualLength = actual.LengthSquared();

			Assert::AreEqual(actualLength, expected);
		}
		TEST_METHOD(TestVec2Dot) {
			Vec2 actualVecOne = { 1, 1 };
			float expected = 2;

			Assert::AreEqual(actualVecOne.Dot(actualVecOne), expected);
		}
		TEST_METHOD(TestVec2Cross) {
			Vec2 actualVecOne = { 1, 1 };
			float expected = 0;

			Assert::AreEqual(actualVecOne.Cross(actualVecOne), expected);

			actualVecOne = { 2, 1 };
			expected = 3;

			Assert::AreEqual(actualVecOne.Cross(actualVecOne), expected);
		}
	};
}